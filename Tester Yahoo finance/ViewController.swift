//  ViewController.swift
//  Ajouter ceci dans le fichier info.plist
/*
 <key>NSAppTransportSecurity</key>
 <dict>
 <key>NSAllowsArbitraryLoads</key>
 <true/>
 </dict>
 */

import UIKit

class ViewController: UIViewController {

    private var compteur = 0
    
    // let URLYahooFinance = "http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.quotes%20where%20symbol%20in%20%28%22YHOO%22%2C%22AAPL%22%2C%22GOOG%22%2C%22MSFT%22%29%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json"

    // requete = http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol in ("MSFT","FB","INTC","HPQ","AAPL","AMD","COKE")&env=http:/datatables.org/alltables.env&format=json
    
    
    @IBOutlet weak var monPortefeuille: UITextView!
    private var dic_resultats  = Dictionary<String, Any>()
    private var _listeDesItems = Array<Dictionary<String, Any>>()

    
    // URL vers l'API finance de Yahoo
    let YahooFinanceURLpart1    = "http://query.yahooapis.com/v1/public/yql?q="
    let requeteSQL              = "select * from yahoo.finance.quotes where symbol in ("
    let porteFeuille = "\"MSFT\",\"FB\",\"INTC\",\"HPQ\",\"AAPL\",\"AMD\",\"COKE\""
    let YahooFinanceURLpart2    = ")&env=http://datatables.org/alltables.env&format=json"
    var URLYahooFinance         = ""

    func obtenirLesDonnées(_ url:String) {
        compteur += 1
        let uneURL = URL(string: url)!  //Danger!
        
        /// Exécuter le traitement suivant en parallèle
        DispatchQueue.main.async ( execute: {
        if let _données = NSData(contentsOf: uneURL) as? Data {
            do {
                let json = try JSONSerialization.jsonObject(with: _données, options: JSONSerialization.ReadingOptions()) as? Dictionary<String, Dictionary<String, Any>>
                
                print("Conversion JSON réussie")
                self.dic_resultats = json!
                //print(self.dic_resultats)
                // Créer un tableau à partir du champ 'resultats'
                if let listeDesItems = ((self.dic_resultats["query"] as? Dictionary<String, Any>)?["results"]as? Dictionary<String, Any>)?["quote"] as? Array<Dictionary<String, Any>> {
                    self._listeDesItems = listeDesItems
                    print("Liste des items:\n\(self._listeDesItems)")
                    self.monPortefeuille.text = self.afficherActions()
                    // self.collectionDesItems.reloadData()
                }
                // print(json)
            } catch {
                print("\n\n#Erreur: Problème de conversion json:\(error)\n\n")
            } // do/try/catch
        } else
        {
            print("\n\n#Erreur: impossible de lire les données via:\(self.URLYahooFinance)\n\n")
        } // if let _données = NSData
      }) // DispatchQueue.main.async
    } // obtenirLesDonnées(_ url:String)
    
    func afficherActions() -> String {
        var res = "-------------------------------------------------\n"
        res += "Lecture numéro: \(self.compteur)\n"
        
        for action in _listeDesItems{
            if let _nom = action["Name"], let _prix = action["Ask"] {
                res += "Action: \(_nom), prix: \(_prix)\n"
            }
        } // for action in
        res += "-------------------------------------------------\n"
        return res
    } // afficherActions()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Construire l'URL de l'API Yahoo à partir du portefeuille local
        URLYahooFinance = YahooFinanceURLpart1 + (requeteSQL + porteFeuille).addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)! + YahooFinanceURLpart2
        print(URLYahooFinance)
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.doTimer), userInfo: nil, repeats: true)
        obtenirLesDonnées(URLYahooFinance)
    } // viewDidLoad()

    func doTimer(){
        obtenirLesDonnées(URLYahooFinance)
    }
}

